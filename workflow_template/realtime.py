"""
<WORKFLOW> workflow
"""

import logging
import ska_ser_logging

from ska_sdp_workflow import workflow

ska_ser_logging.configure_logging()
LOG = logging.getLogger("<LOG>")
LOG.setLevel(logging.DEBUG)

# Claim processing block
pb = workflow.ProcessingBlock()

# Get parameters from processing block.
parameters = pb.get_parameters()
length = parameters.get("length", 3600.0)

# Create work phase
LOG.info("Creating work phase")
work_phase = pb.create_phase("Work", [])

with work_phase:

    LOG.info("Pretending to deploy execution engine.")
    LOG.info("Done, now idling...")

    for txn in work_phase.wait_loop():
        if work_phase.is_sbi_finished(txn):
            break
        txn.loop(wait=True)

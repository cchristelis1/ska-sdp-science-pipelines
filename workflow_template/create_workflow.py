"""
Create source files for real-time or batch workflow.

Usage:
    create_workflow.py <kind> <name>
    create_workflow.py (-h|--help)

Arguments:
    <kind>    Kind of workflow (realtime or batch)
    <name>    Name of the workflow to be created

"""
import os

from docopt import docopt

# Templates
REQUIREMENTS = """--index-url https://artefact.skao.int/repository/pypi-all/simple 
ska-ser-logging 
ska-sdp-workflow
"""

DOCKERFILE = """FROM python:3.9-slim

RUN apt-get update && apt-get -y install gcc

COPY requirements.txt ./
RUN pip install -r requirements.txt

WORKDIR /app
COPY <WORKFLOW-PY> ./
ENTRYPOINT ["python", "<WORKFLOW-PY>"]
"""

MAKEFILE = """NAME := <WORKFLOW-NAME>
VERSION := $(shell cat version.txt)

include ../../make/Makefile
"""

README = "# <WORKFLOW-NAME> workflow"

VERSION = "0.0.0"


def create_source_file(template_file, workflow_name, workflow_name_ftd, output_file):
    """
    Create the source file.

    :param template_file: Template of the source file
    :param workflow_name: Name of the workflow
    :param workflow_name_ftd: Formatted workflow name
    :param output_file: Output file

    """
    # Read source template file
    with open(template_file, "r") as file:
        filedata = file.read()

    # Replace the target strings
    wflow_name = filedata.replace("<WORKFLOW>", workflow_name_ftd)
    filedata = wflow_name.replace("<LOG>", workflow_name)

    # Write the file out again
    with open(output_file, "w") as file:
        file.write(filedata)


if __name__ == "__main__":

    args = docopt(__doc__, options_first=True)

    # Initialise variables
    workflow_name = args["<name>"]
    workflow_name_ftd = workflow_name.replace("_", " ").capitalize()
    workflow_file_name = workflow_name + ".py"

    template_dir = os.path.dirname(__file__)
    parent_dir = os.path.dirname(template_dir)
    source_path = os.path.join(parent_dir, "src", workflow_name)

    print("*** Start generating source files ***\n")

    # Create the directory
    try:
        os.mkdir(source_path)
        print("Directory '%s' created" % workflow_name)
    except FileExistsError:
        print("Workflow already exists!")
        exit()

    # Create source file
    template_file = os.path.join(template_dir, args["<kind>"] + ".py")
    output_file = os.path.join(source_path, workflow_file_name)
    create_source_file(template_file, workflow_name, workflow_name_ftd, output_file)
    print("Source file %s created " % workflow_file_name)

    # Create version.txt file
    with open(os.path.join(source_path, "version.txt"), "w") as fp:
        fp.write(VERSION)
    print("version.txt file created")

    # Create README.md
    readme = README.replace("<WORKFLOW-NAME>", workflow_name_ftd)
    with open(os.path.join(source_path, "README.md"), "w") as fp:
        fp.write(readme)
    print("README.md file created")

    # Create requirements.txt file
    with open(os.path.join(source_path, "requirements.txt"), "w") as fp:
        fp.write(REQUIREMENTS)
    print("requirements.txt file created")

    # Create Dockerfile
    dockerfile = DOCKERFILE.replace("<WORKFLOW-PY>", workflow_file_name)
    with open(os.path.join(source_path, "Dockerfile"), "w") as fp:
        fp.write(dockerfile)
    print("Dockerfile file created")

    # Create Makefile
    wflow_str = workflow_name.replace("_", "-")
    wflow_name = "ska-sdp-wflow-" + wflow_str
    makefile = MAKEFILE.replace("<WORKFLOW-NAME>", wflow_name)
    with open(os.path.join(source_path, "Makefile"), "w") as fp:
        fp.writelines(makefile)
    print("Makefile file created")

    print("\n*** Finished generating source files ***")
